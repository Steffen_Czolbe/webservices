#!/usr/bin/env python
import web
import simplejson as json
import helpers

#connect to DB
db = web.database(dbn='sqlite', db='appointmentDB')

#operations on /poll/ID/vote/ID
class votes:
    #edits a vote
    def PUT(self, pollID, voteID):
        if not helpers.poll_exist(pollID):
            web.ctx.status = '401 no such poll'
            return
        if not helpers.vote_exist(voteID):
            web.ctx.status = '401 no such vote'
            return
        #read request body
        try:
            reqBody = json.loads(web.data())
        except:
            web.ctx.status = '400 body JSON format faulty'
            return
        
        #check if editkey is specified
        votes = db.query("SELECT * FROM vote WHERE id="+voteID)
        vote = votes.list()[0]
        #read editkey from html parameters
        if not helpers.isset(web.input(), "editkey"):
            web.ctx.status = "401 keys don't match" #parameter not specified
            return
        editkey = web.input()["editkey"]
            
        #check if editkey matches
        if not vote["editkey"] == editkey:
            web.ctx.status = "401 keys don't match" #edit key not correct
            return
            
        #update name if set
        if helpers.isset(reqBody, "name"):
            db.update('vote', where= "id = " + voteID, name = reqBody["name"])
            
        #delete old vote appointment association
        db.delete('vote_on_appointment', where="FK_vote=" + voteID)
        
        #write new appointments
        if helpers.isset(reqBody, "appointments"):
            for appointment in reqBody["appointments"]:
                    if helpers.isset(appointment, "id"):
                        db.insert('vote_on_appointment', FK_vote=voteID, FK_appointment=appointment["id"], FK_poll=pollID)
        return
        
    #delets a vote
    def DELETE(self, pollID, voteID):
        if not helpers.poll_exist(pollID):
            web.ctx.status = '401 no such poll'
            return
        if not helpers.vote_exist(voteID):
            web.ctx.status = '401 no such vote'
            return
        
        #check if editkey is specified
        votes = db.query("SELECT * FROM vote WHERE id="+voteID)
        vote = votes.list()[0]
        #read editkey from html parameters
        if not helpers.isset(web.input(), "editkey"):
            web.ctx.status = "401 keys don't match" #parameter not specified
            return
        editkey = web.input()["editkey"]
            
        #check if editkey matches
        if not vote["editkey"] == editkey:
            web.ctx.status = "401 keys don't match" #edit key not correct
            return
            
        #delete votes
        db.delete('vote', where="id=" + str(vote["id"]))
        #delete vote appointment association
        db.delete('vote_on_appointment', where="FK_vote="+str(vote["id"]))
        
        return
        
          
