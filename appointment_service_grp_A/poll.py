#!/usr/bin/env python
import web
import simplejson as json
from helpers import poll_exist
from helpers import isset

#connect to DB
db = web.database(dbn='sqlite', db='appointmentDB')

#operations on /poll/ID
class poll:     
    #returns one poll, including appointments and votes on them   
    def GET(self, id):
        #check if poll exists
        if not poll_exist(id):
            web.ctx.status = '404 no such poll'
            return
            
        #read poll
        polls = db.query("SELECT * FROM poll WHERE id="+id)
        poll = polls.list()[0]
        #read appointments
        res = db.query("SELECT id, start, duration FROM appointment WHERE FK_poll="+str(poll["id"]))
        appointments = res.list()
        poll["appointments"] = []
        for appointment in appointments:
            #read votes
            res = db.query("SELECT V.id, V.name FROM appointment AS A JOIN vote_on_appointment AS VA ON A.id=VA.FK_appointment JOIN vote AS V ON V.id=VA.FK_vote WHERE A.id="+str(appointment["id"]))
            appointment["voters"]  = res.list()
            appointment["votes"]  = len(appointment["voters"])
            poll["appointments"].append(appointment)
        return json.dumps(poll, indent=4, separators=(',', ': '))
        
            
    #updates a poll and appointments
    def PUT(self, id):
        #check if pol exists
        if not poll_exist(id):
            web.ctx.status = "401 keys don't match" 
            return
        #check if editkey is specified
        polls = db.query("SELECT * FROM poll WHERE id="+id)
        poll = polls.list()[0]
        #read editkey from html parameters
        if not isset(web.input(), "editkey"):
            web.ctx.status = "401 keys don't match" #parameter not specified
            web.debug("DEBUG: editkey not set")
            return
        editkey = web.input()["editkey"]
        web.debug("DEBUG: editkey is " + web.input()["editkey"])
            
        #check if editkey matches
        if not poll["editkey"] == editkey:
            web.ctx.status = "401 keys don't match" #edit key not correct
            return
        
        #read request body
        try:
            req = json.loads(web.data())
        except:
            web.ctx.status = '400 body JSON format faulty'
            return
        
        #check other input
        if not isset(req, "appointments"):
            web.ctx.status = "404 missing data"
            return
        
        #read requested appointments
        appointments_req = req["appointments"]
        
        #validate appointments
        for appointment in appointments_req:
            if not ((isset(appointment, "start") and isset(appointment, "duration") or isset(appointment, "id"))):
                web.ctx.status = "404 missing data"
                return
        
        #update name
        if isset(req, "name"):
            db.update('poll', where="id = " + id, name = req["name"])
            
        #read current appointments
        appointments_prev = db.query("SELECT * FROM appointment WHERE FK_poll="+id).list()
        
        #list of apointments that should be deleted
        appointments_delete = []
        #list of appointments that should be altered
        appointments_change = []
        #list of appointments that should be newly created
        appointments_new = []
        
        for appointment_prev in appointments_prev:
            found = False
            for appointment_req in appointments_req:
                if (isset(appointment_req, "id") and appointment_req["id"] == appointment_prev["id"]):
                    found = True
                if ((isset(appointment_req, "id") and appointment_req["id"] == appointment_prev["id"]) and (isset(appointment_req, "start") or isset(appointment_req, "duration"))):
                    appointments_change.append(appointment_req)
            if not found:
                appointments_delete.append(appointment_prev)
        
        
        for appointment_req in appointments_req:
            if not isset(appointment_req, "id"):
                appointments_new.append(appointment_req)
                
        #delete appointments and votes on them
        for appointment in appointments_delete:
            #delete appointment
            db.delete('appointment', where="id="+str(appointment["id"]))
            #delete vote appointment association
            db.delete('vote_on_appointment', where="FK_appointment="+str(appointment["id"]))
            
        #update appointents
        for appointment in appointments_change:
            if isset(appointment, "start") and isset(appointment, "duration"):
                db.update('appointment', where="id = "+str(appointment["id"]), start = appointment["start"], duration = appointment["duration"])
            elif isset(appointment, "start"):
                db.update('appointment', where="id = "+str(appointment["id"]), start = appointment["start"])
            else:
                db.update('appointment', where="id = "+str(appointment["id"]), duration = appointment["duration"])
                
        #create new appointments
        for appointment in appointments_new:
            db.insert('appointment', FK_poll=id, start=appointment["start"], duration=appointment["duration"])
            
        return
        
        
    #deletes a poll, appointments and votes
    def DELETE(self, id):
        #check if pol exists
        if not poll_exist(id):
            web.ctx.status = "401 keys don't match" 
            return
            
        #check if editkey is specified
        polls = db.query("SELECT * FROM poll WHERE id="+id)
        poll = polls.list()[0]
        #read editkey from html parameters
        if not isset(web.input(), "editkey"):
            web.ctx.status = "401 keys don't match" #parameter not specified
            return
        editkey = web.input()["editkey"]
            
        #check if editkey matches
        if not poll["editkey"] == editkey:
            web.ctx.status = "401 keys don't match" #edit key not correct
            return
        
        #delete poll
        db.delete('poll', where="id="+str(poll["id"]))
        #delete appointments
        db.delete('appointment', where="FK_poll="+str(poll["id"]))
        #delete votes
        db.delete('vote', where="FK_poll="+str(poll["id"]))
        #delete vote appointment association
        db.delete('vote_on_appointment', where="FK_poll="+str(poll["id"]))
