#!/usr/bin/env python
#helper functions

def is_int(string):
    try:
        i = int(string)
        return i >= 0
    except ValueError:
        return False
        
#checks, if a key is set for an object
def isset(obj, key):
    try:
        obj[key]
    except KeyError:
        return False
    else:
        return True