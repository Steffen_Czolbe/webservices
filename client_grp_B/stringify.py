#!/usr/bin/env python
#This component of a client implementing webservice grp B helps to stringify json objects to user-readable text
import helpers
import sys

#prints a json-format poll to std out
def poll(poll):
    print 'Poll No.' + poll["id"] + ": " + poll["name"]
    if helpers.isset(poll, "adminid"):
        print 'AdminID: ' + poll["adminid"]
    if helpers.isset(poll, "termine"):
        print 'appointments:'
        for appointment in poll["termine"]:
            if helpers.isset(appointment, "id"):
                sys.stdout.write('    No. ' + str(appointment["id"]))
            if helpers.isset(appointment, "date"):
                sys.stdout.write(' date: ' + appointment["date"] +'\n')
            sys.stdout.flush()
    if helpers.isset(poll, "teilnehmer"):
        print 'participants:'
        for participant in poll["teilnehmer"]:
            if helpers.isset(participant, "id"):
                sys.stdout.write('    No. ' + str(participant["id"]))
            if helpers.isset(participant, "name"):
                sys.stdout.write(' name: ' + participant["name"] +'\n')
            if helpers.isset(participant, "termine"):
                sys.stdout.write('\n        appointments: ')
                for termin in participant["termine"]:
                    sys.stdout.write(str(termin) + " ")
            sys.stdout.write('\n')
            sys.stdout.flush()