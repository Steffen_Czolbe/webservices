#!/usr/bin/env python
#This is the server-communications component of a client implementing webservice grp B
import simplejson as json
import httplib
import helpers

#gets a poll with the specified id (no admin id) and all associated data
def poll_get_normal(host, id):
    conn = httplib.HTTPConnection(host)
    #read poll
    conn.request("GET", "/umfragen/" + str(id))
    response = conn.getresponse()
    ret = {}
    ret["status"] = str(response.status)
    ret["reason"] = response.reason
    if ret["status"] == "200":
        ret["poll"] = json.loads(response.read())
        if helpers.isset(ret["poll"], "termine"):
            #read termine
            conn.request("GET", "/umfragen/" + str(id) + "/termine")
            response = conn.getresponse()
            #merge termine to poll (retarted api, relying on order of termine in list?!?)
            if response.status == "200":
                terminidlist = ret["poll"]["termine"]
                termindatelist = json.loads(response.read())["termine"]
                ret["poll"]["termine"] = {}
                i = 0
                for termin in ret["poll"]["termine"]:
                    ret["poll"]["termine"]["id"] = terminidlist[i]
                    ret["poll"]["termine"]["date"] = termindatelist[i]
                    i += 1
    print json.dumps(ret)
    return ret